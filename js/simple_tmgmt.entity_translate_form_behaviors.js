(function ($, Drupal) {
  'use strict';

  /**
   * Drupal behaviors for the Simple Translation Management module.
   *
   * Applies to the entity translate form.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.simpleTmgmtEntityTranslateForm = {
    attach: function (context) {
      this.listenLanguageCheckboxes(context);
    },

    /**
     * Display the operation links only when there is no more than one language
     * selected.
     *
     * @param {HTMLElement|jQuery} [context=document]
     *   The context which to find elements in.
     */
    listenLanguageCheckboxes: function (context) {
      var $context = $(context || document);
      var $languagesTable = $context.find('table[data-drupal-selector="edit-languages"]');
      var operationWidgets = [];
      $languagesTable.find('.dropbutton-widget').one().each(function () {
        operationWidgets.push($(this));
      });
      var checkedLanguages = 0;

      $languagesTable.find('input[type="checkbox"]').once('simpleTmgmtEntityTranslateForm').each(function () {
        var $languageCheckBox = $(this);

        $languageCheckBox.change(function() {
          if($languageCheckBox.is(':checked')){
            checkedLanguages++;
          }
          else if (checkedLanguages > 0) {
            checkedLanguages--;
          }

          if (checkedLanguages > 1) {
            $.each(operationWidgets, function(index, value){
              $(value).hide();
            });
          }
          else {
            $.each( operationWidgets, function(index, value){
              $(value).show();
            });
          }
        // trigger in case of form state alteration.
        }).trigger('change');
      });
    },
  };

}(jQuery, Drupal));
