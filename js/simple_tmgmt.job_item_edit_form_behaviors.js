(function ($, Drupal) {
  'use strict';

  /**
   * Drupal behaviors for the Simple Translation Management module.
   *
   * Applies to the job item edit form.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.simpleTmgmtEntityTranslateForm = {
    attach: function () {
      // Makes the tick boxes status more obvious.
      // Context is not necessary
      // .once() might need a listener so keep it simple.
      $('input[data-drupal-selector*="-reviewed"]').prop('title', Drupal.t('Mark as reviewed')).css('color', 'red').val('X');
      $('input[data-drupal-selector*="-unreviewed"]').prop('title', Drupal.t('Mark as not reviewed')).css('color', 'green').css('text-decoration', 'none');
    },
  };

}(jQuery, Drupal));
