<?php

namespace Drupal\simple_tmgmt_deepl;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt_deepl\DeeplProTranslatorUi as TmgmtDeeplProTranslatorUi;

/**
 * Machine translator UI, based on DeepL.
 */
class DeeplTranslatorUi extends TmgmtDeeplProTranslatorUi {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function checkoutSettingsForm(array $form, FormStateInterface $form_state, JobInterface $job) {
    $config = \Drupal::configFactory()->get('simple_tmgmt.settings');
    $privacyDescription = '<p>' . $config->get('privacy_agreement_description');
    $privacyUrl = $config->get('privacy_agreement_url');
    if (!empty($privacyUrl)) {
      $link = Link::fromTextAndUrl(
        $this->t('Read more'),
        Url::fromUri($privacyUrl, [
          'attributes' => [
            'target' => '_blank',
          ],
        ])
      )->toRenderable();
      $privacyDescription .= ' ' . \Drupal::service('renderer')->render($link);
    }
    $privacyDescription .= '</p>';
    $form['privacy_description'] = [
      '#markup' => $privacyDescription,
    ];
    return parent::checkoutSettingsForm($form, $form_state, $job);
  }

}
