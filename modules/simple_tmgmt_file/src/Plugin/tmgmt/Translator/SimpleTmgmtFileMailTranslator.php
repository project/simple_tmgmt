<?php

namespace Drupal\simple_tmgmt_file\Plugin\tmgmt\Translator;

use Drupal\Core\File\FileSystemInterface;
use Drupal\simple_tmgmt\SimpleTmgmtInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\file\FileUsage\FileUsageInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt_file\Plugin\tmgmt\Translator\FileTranslator;
use Drupal\simple_tmgmt\Plugin\tmgmt\Translator\ManualTranslatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Simple TMGMT manual file translator override.
 *
 * @TranslatorPlugin(
 *   id = "simple_tmgmt_file_mail",
 *   label = @Translation("File exchange by mail"),
 *   description = @Translation("Sends a File (xlf, html) by mail and optionally adds a deadline."),
 *   ui = "Drupal\simple_tmgmt\ManualTranslatorUi"
 * )
 */
class SimpleTmgmtFileMailTranslator extends FileTranslator implements ManualTranslatorInterface, ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\simple_tmgmt\SimpleTmgmt definition.
   *
   * @var \Drupal\simple_tmgmt\SimpleTmgmt
   */
  protected $simpleTmgmt;

  /**
   * Drupal\Core\Datetime\DateFormatterInterface definition.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Drupal\file\FileUsage\FileUsageInterface definition.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  protected $fileUsage;

  /**
   * Simple Tmgmt File constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\simple_tmgmt\SimpleTmgmtInterface $simple_tmgmt
   * @param \Drupal\file\FileUsage\FileUsageInterface $file_usage
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $config_factory,
    SimpleTmgmtInterface $simple_tmgmt,
    FileUsageInterface $file_usage,
    DateFormatterInterface $date_formatter
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->simpleTmgmt = $simple_tmgmt;
    $this->fileUsage = $file_usage;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('simple_tmgmt'),
      $container->get('file.usage'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function requestTranslation(JobInterface $job) {
    $libXmlPreviousState = libxml_use_internal_errors(TRUE);
    $name = 'Job_ID_' . $job->id() . '-' . $job->getSourceLangcode() . '_' . $job->getTargetLangcode();

    $export = \Drupal::service('plugin.manager.tmgmt_file.format')->createInstance($job->getSetting('export_format'), $job->getSetting('format_configuration'));

    $path = $job->getSetting('scheme') . '://tmgmt_file/' . $name . '.' . $job->getSetting('export_format');
    $dirname = dirname($path);
    if (\Drupal::service('file_system')->prepareDirectory($dirname, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      $file = file_save_data($export->export($job), $path, FileSystemInterface::EXISTS_REPLACE);
      $this->fileUsage->add($file, 'tmgmt_file', 'tmgmt_job', $job->id());
      $job->submitted($this->t('Exported file can be <a href="@url" download>downloaded</a>.', ['@url' => file_create_url($path)]));

      if ($job->getSetting('send_mail')) {
        // @todo check multiple items.
        $jobItems = $job->getItems();
        reset($jobItems);
        /** @var \Drupal\tmgmt\JobItemInterface $jobItem */
        $jobItemKey = key($jobItems);
        $jobItem = $jobItems[$jobItemKey];

        // Format delivery date.
        $systemDateConfig = $this->configFactory->get('system.date');
        $timeZone = $systemDateConfig->get('timezone.default');
        $deliveryDate = new DrupalDateTime($job->getSetting('delivery_date'), $timeZone);
        $deliveryDateFormat = $this->dateFormatter->format($deliveryDate->getTimestamp(), 'custom', 'd/m/Y');

        $config = $this->configFactory->get('simple_tmgmt.settings');

        // Add Job link.
        $jobItemUrl = Url::fromRoute('entity.tmgmt_job_item.canonical', [
          'tmgmt_job_item' => $jobItem->id(),
        ]);
        $jobUrl = Url::fromRoute('entity.tmgmt_job.canonical', [
          'tmgmt_job' => $job->id(),
        ])
          ->setOptions([
            'query' => [
              'destination' => $jobItemUrl->toString(),
            ],
          ])
          ->setAbsolute(TRUE);
        $jobLinkLabel = $config->get('translation_service_mail_link_label');
        $jobLink = Link::fromTextAndUrl($jobLinkLabel, $jobUrl)->toRenderable();
        $jobUploadMarkup = \Drupal::service('renderer')->render($jobLink);

        $content = [
          'node_title' => $jobItem->getSourceLabel(),
          'source_language' => strtoupper($job->getSourceLanguage()->getId()),
          'target_language' => strtoupper($job->getTargetLanguage()->getId()),
          'words' => $job->getWordCount(),
          'has_delivery_date' => $job->getSetting('has_delivery_date'),
          'delivery_date' => $deliveryDateFormat,
          'file_uri' => $file->getFileUri(),
          'file_name' => $file->getFilename(),
          'job_link' => $jobUploadMarkup,
        ];

        $mail = $job->getSetting('mail');
        $this->simpleTmgmt->sendJobCreateMail($content, $mail);
      }
    }
    else {
      $job->rejected('Failed to create writable directory @dirname, check file system permissions.', ['@dirname' => $dirname]);
    }
    // Set back initial libxml state.
    libxml_clear_errors();
    libxml_use_internal_errors($libXmlPreviousState);
  }

  /**
   * {@inheritdoc}
   */
  public function hasCheckoutSettings(JobInterface $job) {
    // Always true as the delivery date is required.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    // Default to xlf.
    return [
      'export_format' => 'xlf',
      'allow_override' => TRUE,
      'scheme' => 'public',
      'xliff_processing' => TRUE,
      'xliff_cdata' => TRUE,
      'format_configuration' => [],
    ];
  }

}
