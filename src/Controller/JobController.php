<?php

namespace Drupal\simple_tmgmt\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\tmgmt\TMGMTException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Route;

/**
 * Class JobController.
 */
class JobController extends ControllerBase {

  /**
   * Creates a TMGMT Job and redirects to the created Job entity.
   *
   * @param string $entity_type_id
   * @param string $entity_id
   * @param string $langcode
   * @param string $default_provider
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function createJob($entity_type_id, $entity_id, $langcode, $default_provider) {
    // The default destination is the translation overview form of the entity.
    /** @var \Drupal\Core\Routing\RouteProviderInterface $routeProvider */
    $routeProvider = \Drupal::service('router.route_provider');
    $entityTranslationFormRoute = 'entity.' . $entity_type_id . '.content_translation_overview';
    try {
      $route = $routeProvider->getRouteByName($entityTranslationFormRoute);
      if ($route instanceof Route) {
        $destinationUrl = Url::fromRoute($entityTranslationFormRoute, [$entity_type_id => $entity_id]);
      }
      $storage = $this->entityTypeManager()->getStorage($entity_type_id);
      $entity = $storage->load($entity_id);
      $job = tmgmt_job_create($entity->language()->getId(), $langcode, \Drupal::currentUser()->id());
      try {
        // Add the job item.
        $item = $job->addItem('content', $entity->getEntityTypeId(), $entity->id());
        // Override the destination Url for Machine translators, it should lead to the
        // Job Item edit form.
        /** @var \Drupal\simple_tmgmt\SimpleTmgmtInterface $simpleTmgmt */
        $simpleTmgmt = \Drupal::service('simple_tmgmt');
        if ($simpleTmgmt->isMachineTranslator($default_provider)) {
          $destinationUrl = Url::fromRoute('entity.tmgmt_job_item.canonical', [
            'tmgmt_job_item' => $item->id(),
          ]);
        }
        $tmgmtJobUrl = Url::fromRoute('entity.tmgmt_job.canonical', ['tmgmt_job' => $job->id()], [
          'query' => [
            'default_provider' => $default_provider,
            'destination' => $destinationUrl->toString(),
          ],
        ]);
        return new RedirectResponse($tmgmtJobUrl->toString());
      }
      catch (TMGMTException $exception) {
        $this->loggerFactory->get('simple_tmgmt')->error($exception->getMessage());
        $languages = $this->languageManager()->getLanguages();
        $targetLangName = $languages[$langcode]->language;
        $this->messenger()->addError($this->t('Unable to add job item for target language %name. Make sure the source content is not empty.', ['%name' => $targetLangName]));
      }
    }
    catch (\Exception $exception) {
      $this->getLogger('simple_tmgmt')->error($exception->getMessage());
      $this->messenger()->addError($exception->getMessage());
    }
    return new RedirectResponse($destinationUrl->toString());
  }

}
