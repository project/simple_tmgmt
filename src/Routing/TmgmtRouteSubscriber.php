<?php

namespace Drupal\simple_tmgmt\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Adds custom TMGMT permissions.
 */
class TmgmtRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    // Add the 'delete job' permission.
    // This is mostly done to prevent to set the broad
    // 'administer tmgmt' permission only to delete jobs.
    if ($route = $collection->get('entity.tmgmt_job.delete_form')) {
      // Make sure that at least the job update entity access is granted,
      // then add the custom delete job permission.
      $route->setRequirement('_entity_access', 'tmgmt_job.update');
      $route->setRequirement('_permission', 'delete translation jobs');
    }

    // Add the custom 'access tgmt overview' permission to "Overview"
    // related routes.
    $routes = [
      'tmgmt.source_overview_default',
      'view.tmgmt_job_overview.page_1',
      'view.tmgmt_translation_all_job_items.page_1',
      'entity.tmgmt_translator.collection',
    ];
    foreach ($routes as $route) {
      if ($route = $collection->get($route)) {
        $route->setRequirement('_permission', 'access tmgmt overview');
      }
    }
  }

}
