<?php

namespace Drupal\simple_tmgmt\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\tmgmt\JobItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Confirmation form for the Job deletion.
 */
class JobDeleteConfirmForm extends ConfirmFormBase {

  /**
   * Entity type id of the Job Item.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * Entity id of the Job Item.
   *
   * @var int
   */
  protected $entityId;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * JobDeleteConfirmForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type_id = NULL, $entity_id = NULL) {
    $this->entityTypeId = $entity_type_id;
    $this->entityId = $entity_id;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Delete inactive Job Items and their related Jobs.
    try {
      $tmgmtJobItemStorage = $this->entityTypeManager->getStorage('tmgmt_job_item');
      $tmgmtJobStorage = $this->entityTypeManager->getStorage('tmgmt_job');
      $inactiveJobItems = $tmgmtJobItemStorage->loadByProperties([
        'state' => JobItemInterface::STATE_INACTIVE,
        'item_type' => $this->entityTypeId,
        'item_id' => $this->entityId,
      ]);
      if (!empty($inactiveJobItems)) {
        /** @var \Drupal\tmgmt\JobItemInterface $inactiveJobItem */
        foreach ($inactiveJobItems as $inactiveJobItem) {
          $jobIds[] = $inactiveJobItem->getJobId();
        }
        $jobs = $tmgmtJobStorage->loadMultiple($jobIds);
        $tmgmtJobItemStorage->delete($inactiveJobItems);
        $tmgmtJobStorage->delete($jobs);
      }
    }
    catch (\Exception $exception) {
      $this->messenger()->addError($exception->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "job_delete_confirm_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    // Could be extended to other entity types.
    return new Url('entity.node.content_translation_overview', [
      'node' => $this->entityId,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to delete the <em>Inactive</em> translation Job items?');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    try {
      $item = $this->entityTypeManager->getStorage($this->entityTypeId)->load($this->entityId);
      $title = $item->label();
    }
    catch (\Exception $exception) {
      $this->messenger()->addError($exception->getMessage());
      return $this->t('There was an error while processing the entity @entity_type_id @entity_id', [
        '@entity_type_id' => $this->entityTypeId,
        '@entity_id' => $this->entityId,
      ]);
    }
    return $this->t('This will delete the <em>Inactive</em> Job items for the @entity_type_id <em>@title</em> and cannot be undone.',
      [
        '@title' => $title,
        '@entity_type_id' => $this->entityTypeId,
      ]
    );
  }

}
