<?php

namespace Drupal\simple_tmgmt\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Render\RendererInterface definition.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Render\RendererInterface $renderer
   */
  public function __construct(ConfigFactoryInterface $config_factory, RendererInterface $renderer) {
    parent::__construct($config_factory);
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'simple_tmgmt.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simple_tmgmt_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $testMailLinkUrl = Url::fromRoute('simple_tmgmt.test_mail');
    $testMailLink = Link::fromTextAndUrl($this->t('Test email'), $testMailLinkUrl)->toRenderable();
    $testMailLinkMarkup = $this->renderer->render($testMailLink);

    $config = $this->config('simple_tmgmt.settings');

    $form['ui'] = [
      '#type' => 'details',
      '#title' => $this->t('User interface'),
      '#open' => TRUE,
    ];

    // @todo split this form in submodules
    // @todo add configuration for most of the components (see roadmap)

    $form['ui']['manual_translation'] = [
      '#type' => 'details',
      '#title' => $this->t('Manual translation'),
      '#open' => TRUE,
    ];
    $form['ui']['manual_translation']['default_job_mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Default job email'),
      '#required' => TRUE,
      '#default_value' => $config->get('default_job_mail'),
      '#description' => $this->t('Default email address that will receive File based translation Jobs, if no email is provided.'),
    ];
    $form['ui']['manual_translation']['delivery_days'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum delivery days'),
      '#required' => TRUE,
      '#description' => $this->t('Minimum amount of days for the delivery date. Used for delivery date validation.'),
      '#default_value' => $config->get('delivery_days'),
    ];
    $form['ui']['manual_translation']['delivery_date_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delivery date label'),
      '#required' => TRUE,
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('delivery_date_label'),
    ];
    $form['ui']['manual_translation']['delivery_date_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Delivery date description'),
      '#required' => TRUE,
      '#rows' => 3,
      '#default_value' => $config->get('delivery_date_description'),
    ];
    $form['ui']['manual_translation']['translation_service_mail_link_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mail - Job link label'),
      '#required' => TRUE,
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('translation_service_mail_link_label'),
    ];

    $form['ui']['machine_translation'] = [
      '#type' => 'details',
      '#title' => $this->t('Machine translation'),
      '#open' => TRUE,
    ];
    $form['ui']['machine_translation']['error_notification_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Error notification email'),
      '#required' => TRUE,
      '#default_value' => $config->get('error_notification_email'),
    ];
    $form['ui']['machine_translation']['privacy_agreement_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Privacy agreement description'),
      '#required' => TRUE,
      '#rows' => 3,
      '#default_value' => $config->get('privacy_agreement_description'),
    ];
    $form['ui']['machine_translation']['privacy_agreement_url'] = [
      '#type' => 'url',
      '#required' => TRUE,
      '#title' => $this->t('Privacy agreement URL'),
      '#default_value' => $config->get('privacy_agreement_url'),
    ];

    $form['ui']['frontend'] = [
      '#type' => 'details',
      '#title' => $this->t('Frontend'),
      '#open' => FALSE,
    ];
    $form['ui']['frontend']['machine_translation_hint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Machine translation message'),
      '#description' => $this->t('Displayed on the frontend to mention that the content has been translated automatically.'),
      '#required' => TRUE,
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('machine_translation_hint'),
    ];

    $form['ui']['translation_form'] = [
      '#type' => 'details',
      '#title' => $this->t('Translation form'),
      '#open' => FALSE,
    ];
    $form['ui']['translation_form']['translate_again'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Translate again'),
      '#description' => $this->t('Allow to translate a content if it has already been translated.'),
      '#default_value' => $config->get('translate_again'),
    ];
    $form['ui']['translation_form']['operation_add'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Drupal translation'),
      '#description' => $this->t('Replaces the default <em>Translate</em> operation label to distinguish it from other translators.'),
      '#required' => TRUE,
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('operation_add'),
    ];

    $form['ui']['job_forms'] = [
      '#type' => 'details',
      '#title' => $this->t('Job and Job Item forms'),
      '#open' => FALSE,
    ];
    $form['ui']['job_forms']['disabled_actions_job_form'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled actions for Jobs'),
      '#description' => $this->t('Allows to simplify actions on Job forms. If all actions are selected, only the <em>Submit to provider</em> operation will be available.'),
      '#options' => [
        'save' => $this->t('Save job'),
        'delete' => $this->t('Delete'),
      ],
      '#default_value' => $config->get('disabled_actions_job_form'),
    ];
    $form['ui']['job_forms']['disabled_actions_job_item_form'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled actions for Job Items'),
      '#description' => $this->t('Allows to simplify actions on Job Item forms. If all actions are selected, only the <em>Save as completed</em> operation will be available.'),
      '#options' => [
        'save' => $this->t('Save'),
        'validate' => $this->t('Validate'),
        'validate_html' => $this->t('Validate HTML tags'),
        'preview' => $this->t('Preview'),
        'abort_job_item' => $this->t('Abort'),
      ],
      '#default_value' => $config->get('disabled_actions_job_item_form'),
    ];
    $form['ui']['job_forms']['delete_inactive_jobs_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Delete inactive jobs message'),
      '#required' => TRUE,
      '#rows' => 3,
      '#default_value' => $config->get('delete_inactive_jobs_message'),
    ];
    $form['ui']['job_forms']['delete_inactive_jobs_link_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delete inactive jobs link label'),
      '#required' => TRUE,
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('delete_inactive_jobs_link_label'),
    ];
    $form['ui']['job_forms']['help_link_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Help link label'),
      '#required' => TRUE,
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('help_link_label'),
    ];
    $form['ui']['job_forms']['help_link_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Help link URL'),
      '#required' => FALSE,
      '#description' => $this->t('If no set, defaults to the module help.'),
      '#default_value' => $config->get('help_link_url'),
    ];
    $form['ui']['job_forms']['pending_manual_translation_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pending manual translation label'),
      '#required' => TRUE,
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('pending_manual_translation_label'),
    ];

    $form['test'] = [
      '#type' => 'details',
      '#title' => $this->t('Testing'),
      '#open' => TRUE,
    ];
    $form['test']['mail'] = [
      '#type' => 'markup',
      '#markup' => $testMailLinkMarkup,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('simple_tmgmt.settings')
      ->set('translate_again', $form_state->getValue('translate_again'))
      ->set('operation_add', $form_state->getValue('operation_add'))
      ->set('disabled_actions_job_form', $form_state->getValue('disabled_actions_job_form'))
      ->set('disabled_actions_job_item_form', $form_state->getValue('disabled_actions_job_item_form'))
      ->set('delete_inactive_jobs_message', $form_state->getValue('delete_inactive_jobs_message'))
      ->set('delete_inactive_jobs_link_label', $form_state->getValue('delete_inactive_jobs_link_label'))
      ->set('help_link_label', $form_state->getValue('help_link_label'))
      ->set('help_link_url', $form_state->getValue('help_link_url'))
      ->set('pending_manual_translation_label', $form_state->getValue('pending_manual_translation_label'))
      ->set('default_job_mail', $form_state->getValue('default_job_mail'))
      ->set('delivery_days', $form_state->getValue('delivery_days'))
      ->set('delivery_date_label', $form_state->getValue('delivery_date_label'))
      ->set('delivery_date_description', $form_state->getValue('delivery_date_description'))
      ->set('translation_service_mail_link_label', $form_state->getValue('translation_service_mail_link_label'))
      ->set('error_notification_email', $form_state->getValue('error_notification_email'))
      ->set('privacy_agreement_description', $form_state->getValue('privacy_agreement_description'))
      ->set('privacy_agreement_url', $form_state->getValue('privacy_agreement_url'))
      ->set('machine_translation_hint', $form_state->getValue('machine_translation_hint'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
