<?php

namespace Drupal\simple_tmgmt\Form;

use Drupal\simple_tmgmt\SimpleTmgmtInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TestMailForm.
 */
class TestMailForm extends FormBase {

  /**
   * Drupal\simple_tmgmt\SimpleTmgmtInterface definition.
   *
   * @var \Drupal\simple_tmgmt\SimpleTmgmtInterface
   */
  protected $simpleTmgmt;

  /**
   * TestMailForm constructor.
   *
   * @param \Drupal\simple_tmgmt\SimpleTmgmtInterface $simple_tmgmt
   */
  public function __construct(SimpleTmgmtInterface $simple_tmgmt) {
    $this->simpleTmgmt = $simple_tmgmt;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('simple_tmgmt')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'test_mail_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['mail_address'] = [
      '#type' => 'email',
      '#title' => $this->t('Email address to be used for testing'),
      '#required' => TRUE,
    ];
    $form['mail_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Mail type'),
      '#default_value' => 'job_create',
      '#options' => [
        'job_create' => $this->t('Job create'),
        'job_error' => $this->t('Job error'),
      ],
      '#required' => TRUE,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $mailTo = $form_state->getValue('mail_address');
    $error = FALSE;
    switch ($form_state->getValue('mail_type')) {
      case 'job_create':
        // @todo provide a version with and without file.
        $content = [
          'node_title' => 'Test node title',
          'source_language' => 'DE',
          'target_language' => 'EN',
          'words' => '123',
          'delivery_date' => '01/01/2020',
          'file_uri' => '',
          'file_name' => '',
          'job_link' => 'You can <a href="#">upload the translated file</a>',
        ];
        if ($this->simpleTmgmt->sendJobCreateMail($content, $mailTo)) {
          $this->messenger()->addStatus($this->t('The mail has been sent to @mail.', ['@mail' => $mailTo]));
        }
        else {
          $error = TRUE;
        }
        break;

      case 'job_error':
        $content = [
          'message' => 'Test error message',
          'text' => 'Test source text',
          'error_code' => 400,
          'original_error' => 'Test original error',
          'translator' => 'Test translator id',
          'source_language' => 'DE',
          'target_language' => 'EN',
        ];
        if ($this->simpleTmgmt->sendJobErrorMail($content, $mailTo)) {
          $this->messenger()->addStatus($this->t('The mail has been sent to @mail.', ['@mail' => $mailTo]));
        }
        break;
    }

    if ($error) {
      $this->messenger()->addError($this->t('There was an error while sending your mail to @mail.', ['@mail' => $mailTo]));
    }

  }

}
