<?php

namespace Drupal\simple_tmgmt;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\tmgmt\TranslatorInterface;

/**
 * Interface SimpleTmgmtInterface.
 */
interface SimpleTmgmtInterface {

  /**
   * Returns a list of active translators.
   *
   * @return \Drupal\tmgmt\Entity\Translator[]
   */
  public function getActiveTranslators();

  /**
   * Checks if a translation supports the provider.
   *
   * @param \Drupal\tmgmt\TranslatorInterface $translator
   * @param \Drupal\Core\Language\LanguageInterface $sourceLanguage
   * @param string $targetLangCode
   *
   * @return bool
   */
  public function isTranslationSupported(TranslatorInterface $translator, LanguageInterface $sourceLanguage, $targetLangCode);

  /**
   * Checks if a translator is machine based.
   *
   * @param string $translator
   *   The translator plugin id.
   *
   * @return bool
   */
  public function isMachineTranslator($translator);

  /**
   * Checks if a translator is manual based.
   *
   * @param string $translator
   *   The translator plugin id.
   *
   * @return bool
   */
  public function isManualTranslator($translator);

  /**
   * Sends a mail for a TMGMT Job.
   *
   * @param array $content
   * @param string $to
   *   Mail recipient.
   *
   * @return array
   */
  public function sendJobCreateMail(array $content, $to);

  /**
   * Sends a mail for a TMGMT Job error.
   *
   * @param array $content
   * @param string $to
   *   Mail recipient.
   *
   * @return array
   */
  public function sendJobErrorMail(array $content, $to);

  /**
   * Gets a Job create url.
   *
   * The url destination is a controller that has 2 side effects
   * - creates the Job
   * - redirects to the Job canonical url.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @param string $langcode
   * @param string $default_provider
   *
   * @return \Drupal\Core\Url
   */
  public function getJobCreateUrl(ContentEntityInterface $entity, $langcode, $default_provider);

  /**
   * Adds the entity translation form URL to the form state.
   *
   * Scope: Job delete or edit forms.
   * So the delete operation can redirect to the entity translation form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function setTranslationFormUrl(FormStateInterface $form_state);

  /**
   * Returns the help link markup.
   *
   * @return string
   */
  public function getHelpLinkMarkup();

  /**
   * Returns the Job form disabled actions.
   *
   * @return array
   */
  public function getJobFormDisabledActions();

  /**
   * Returns the Job Item form disabled actions.
   *
   * @return array
   */
  public function getJobItemFormDisabledActions();

  /**
   * Customizes the translation link.
   *
   * Used as a replacement for a route subscriber that is not an option
   * in the Job / Job Items case.
   *
   * @param string $link
   *
   * @return string
   */
  public function transformTranslationLink($link);

}
