<?php

namespace Drupal\simple_tmgmt;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt_file\FileTranslatorUi;

/**
 * Manual translator UI (based on File translator).
 */
class ManualTranslatorUi extends FileTranslatorUi {

  /**
   * Returns the n next week days date based on the current date.
   *
   * @param string $deliveryDays
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   */
  public static function getMinimumDeliveryDate($deliveryDays) {
    $systemDateConfig = \Drupal::config('system.date');
    $timeZone = $systemDateConfig->get('timezone.default');
    $result = DrupalDateTime::createFromTimestamp(time(), $timeZone);
    $result->modify('+' . $deliveryDays . ' weekday');
    return $result;
  }

  /**
   * Formats the date.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   * @param string $format
   *
   * @return string
   */
  public static function formatDate(DrupalDateTime $date, $format) {
    /** @var \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter */
    $dateFormatter = \Drupal::service('date.formatter');
    return $dateFormatter->format($date->getTimestamp(), 'custom', $format);
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutSettingsForm(array $form, FormStateInterface $form_state, JobInterface $job) {
    $config = \Drupal::configFactory()->get('simple_tmgmt.settings');
    $days = $config->get('delivery_days');
    $description = $config->get('delivery_date_description');
    $description = str_replace('@days', $days, $description);
    $minimumDeliveryDate = self::getMinimumDeliveryDate($days);
    $form['send_mail'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send mail'),
      '#description' => $this->t('Send the file by mail.'),
      '#default_value' => TRUE,
    ];
    $form['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      // '#required' => TRUE, // use states.
      '#states' => [
        'visible' => [
          ':input[name="settings[send_mail]"]' => ['checked' => TRUE],
        ],
      ],
      '#default_value' => $config->get('default_job_mail'),
    ];
    $form['has_delivery_date'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Set delivery date'),
      '#states' => [
        'visible' => [
          ':input[name="settings[send_mail]"]' => ['checked' => TRUE],
        ],
      ],
      '#default_value' => FALSE,
    ];
    $form['delivery_date'] = [
      '#type' => 'date',
      '#title' => $config->get('delivery_date_label'),
      '#default_value' => self::formatDate($minimumDeliveryDate, 'Y-m-d'),
      // '#required' => TRUE, // use states.
      '#states' => [
        'visible' => [
          ':input[name="settings[send_mail]"]' => ['checked' => TRUE],
          ':input[name="settings[has_delivery_date]"]' => ['checked' => TRUE],
        ],
      ],
      '#description' => $description,
      '#element_validate' => [[get_class($this), 'validateDeliveryDate']],
    ];
    return parent::checkoutSettingsForm($form, $form_state, $job);
  }

  /**
   * Validates the delivery date.
   *
   * It must be greater than the minimum delivery date.
   *
   * @param array $element
   *   The input element to validate.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateDeliveryDate(array $element, FormStateInterface &$form_state) {
    $dateFormat = 'd/m/Y';
    if (!empty($element['#value'])) {
      $deliveryTimestamp = strtotime($element['#value']);
      $config = \Drupal::configFactory()->get('simple_tmgmt.settings');
      $days = $config->get('delivery_days');
      $minimumDeliveryDate = self::getMinimumDeliveryDate($days);

      if ($deliveryTimestamp < $minimumDeliveryDate->getTimestamp()) {
        $form_state->setError($element, t('The minimum delivery date is @days working days [@date].', [
          '@days' => $days,
          '@date' => self::formatDate($minimumDeliveryDate, $dateFormat),
        ]));
      }
    }
    else {
      $form_state->setError($element, t('The delivery date is invalid. Please enter in @format format or select from calendar', [
        '@format' => $dateFormat,
      ]));
    }
  }

}
