<?php

namespace Drupal\simple_tmgmt\Plugin\tmgmt\Translator;

/**
 * Interface MachineTranslatorInterface.
 *
 * Groups machine based TMGMT translator plugins.
 *
 * @package Drupal\simple_tmgmt\Plugin\tmgmt\Translator
 */
interface MachineTranslatorInterface {

  const translatorType = 'machine';

}
