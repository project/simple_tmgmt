<?php

namespace Drupal\simple_tmgmt\Plugin\tmgmt\Translator;

/**
 * Interface ManualTranslatorInterface.
 *
 * Groups manual TMGMT translator plugins.
 *
 * @package Drupal\simple_tmgmt\Plugin\tmgmt\Translator
 */
interface ManualTranslatorInterface {

  const translatorType = 'manual';

}
