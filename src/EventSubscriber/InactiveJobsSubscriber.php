<?php

namespace Drupal\simple_tmgmt\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\tmgmt\JobItemInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class InactiveJobsSubscriber.
 */
class InactiveJobsSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Render\RendererInterface definition.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new InactiveJobsSubscriber object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    RendererInterface $renderer,
    MessengerInterface $messenger,
    ConfigFactoryInterface $config_factory
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
    $this->messenger = $messenger;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST] = ['kernelRequest', 31];
    return $events;
  }

  /**
   * This method is called when the kernel.request is dispatched.
   *
   * While accessing a node translation page, check if some Job Items
   * are inactive, they could have been canceled in the middle of the process
   * so propose to the user to delete them.
   *
   * This method is favoured to cron as it is more predictable.
   * Auto-delete is not favoured as several translations could be requested
   * for the same node at the same time (several tabs, or different user)
   * and the translation page could be (re)loaded meanwhile.
   *
   * @param \Symfony\Component\EventDispatcher\Event $event
   *   The dispatched event.
   */
  public function kernelRequest(Event $event) {
    if ($event instanceof GetResponseEvent) {
      $request = $event->getRequest();
      $routeName = $request->get('_route');

      // Could be extended to other entity types.
      if ($routeName === 'entity.node.content_translation_overview') {
        /** @var \Drupal\node\NodeInterface $node */
        $node = $request->attributes->get('node');
        try {
          $tmgmtJobItemStorage = $this->entityTypeManager->getStorage('tmgmt_job_item');
          $inactiveJobItems = $tmgmtJobItemStorage->loadByProperties([
            'state' => JobItemInterface::STATE_INACTIVE,
            'item_type' => 'node',
            'item_id' => $node->id(),
          ]);
          if (!empty($inactiveJobItems)) {
            $destinationUrl = Url::fromRoute($routeName, [
              'node' => $node->id(),
            ]);
            $deleteUrl = Url::fromRoute('simple_tmgmt.job_delete', [
              'entity_type_id' => 'node',
              'entity_id' => $node->id(),
            ], [
              'query' => [
                'destination' => $destinationUrl->toString(),
              ],
            ]);
            $config = $this->configFactory->get('simple_tmgmt.settings');
            $deleteLinkLabel = $config->get('delete_inactive_jobs_link_label');
            $deleteLink = Link::fromTextAndUrl($deleteLinkLabel, $deleteUrl)->toRenderable();
            $deleteLinkMarkup = $this->renderer->renderRoot($deleteLink);
            $deleteMessage = Markup::create($config->get('delete_inactive_jobs_message') . ' ' . $deleteLinkMarkup);
            $this->messenger->addWarning($deleteMessage);
          }
        }
        catch (\Exception $exception) {
          $this->messenger->addError($exception->getMessage());
        }
      }
    }
  }

}
