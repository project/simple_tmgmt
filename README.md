# Simple Translation Management

UX experiments to simplify the workflow for basic use cases,
and integration with other modules.

### Integration with other modules

- Swift Mailer: send xlf files as attachments with html template
- Disable Language: filter languages with permissions on the translation form
- DeepL: re-route error messages to a specific email address
and simplify the error messages for content editors.

### UX experiments

- Translate with the usual translate operations links
- Disable operation links when the user selects multiple languages
- Limit operation links to the supported translators
- Optionally disable translation links once already translated (edit only and do not translate again via a Job)
- Per node translation flag to keep track of machine translation
- Optionally add a delivery date for File translation. A default delivery date is calculated based on a certain amount of open days
- If a Job is left as unprocessed allow to continue or delete it via the translation form
- On Job delete or Job Item delete, redirect to the entity translation form
- Add _Access translation management overview_ permission to simplify the UI for some roles
- Add documentation link (help) on the translation form
- Autocomplete the Job title
- Upload file from the translation form if the Job is pending

### Roadmap

This is a series of UX experiments, to gather feedback and that are subject to change / frequent refactoring.
Further changes could include e.g.

- A generic way to deal with notifications, as a submodule. See [#2703851 mail notifications](https://www.drupal.org/project/tmgmt/issues/2703851)
- Introduce a TMGMT Workflow by provider (e.g. Machine workflow, Manual workflow)
- Add a permission or configuration to use the cart
- Add a permission or configuration for Job review validation tick boxes
- Configure form actions (abort, validate, ...) to display/hide them
- Configure Job creation suggestions to display/hide them
- Configure semantic integrity messages to display/hide them
